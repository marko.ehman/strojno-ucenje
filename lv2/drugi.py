import numpy as np
import matplotlib . pyplot as plt

data = np.loadtxt('data.csv',delimiter=',',skiprows=1)
print("Mjerenje je izvršeno na",len(data),"ososba")

plt.scatter(data[:,1],data[:,2],s=2)
plt.title("Zad b")
plt.show()

plt.scatter(data[::50,1],data[::50,2],s=2)
plt.title("Zad c")
plt.show()

print(f"Prosjecna visina je {np.mean(data[:,1]):.2f} cm")
print(f"Maksimalna visina je {np.max(data[:,1]):.2f} cm")
print(f"Minimalna visina je {np.min(data[:,1]):.2f} cm")

m = (data[:,0] == 1)
z = (data[:,0] == 0)

print(f"Prosjecna visina zena je {np.mean(data[z,1]):.2f} cm")
print(f"Maksimalna visina kod zena je {np.max(data[z,1]):.2f} cm")
print(f"Minimalna visina kod zena je {np.min(data[z,1]):.2f} cm")
print(f"Prosjecna visina muskaraca je {np.mean(data[m,1]):.2f} cm")
print(f"Maksimalna visina kod muskaraca je {np.max(data[m,1]):.2f} cm")
print(f"Minimalna visina kod muskaraca je {np.min(data[m,1]):.2f} cm")

