import numpy as np
import matplotlib . pyplot as plt

white = np.ones((50,50))
black = np.zeros((50,50))

first_line = np.hstack((black,white))
second_line = np.hstack((white,black))

picture = np.vstack((first_line,second_line))

plt.imshow(picture, cmap='gray')
plt.show()