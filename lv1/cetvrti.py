words_dict = {}

file = open('song.txt')

for line in file:
    words = line.rstrip().split()
    for word in words:
        if word in words_dict:
            words_dict[word]+=1
        else:
            words_dict[word]=1

unique_words = [word for word in words_dict if words_dict[word] == 1]

print('Number of unique words: ',len(unique_words))
for word in unique_words:
    print(word)

