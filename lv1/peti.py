#a)
spam_words = 0
spam_count = 0
ham_words = 0
ham_count = 0

file = open('SMSSpamCollection.txt')
for line in file:
    category, message = line.rstrip().split(maxsplit=1)
    words = len(message.split())
    if category == 'spam':
        spam_count+=1
        spam_words+=words
    else:
        ham_count+=1
        ham_words+=words

ham_average = ham_words/ham_count
spam_average = spam_words/spam_count

print(f"Ham average: {ham_average:.2f}")
print(f"Spam average: {spam_average:.2f}")

file.close()

#b)

target_messages = 0
file = open('SMSSpamCollection.txt')
for line in file:
    category, message = line.rstrip().split(maxsplit=1)
    if category == 'spam' and message.endswith('!'):
        target_messages+=1
print("Number of sentences ending with exclamation is: ", target_messages)



